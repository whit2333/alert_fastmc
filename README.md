ALERT Fast MC
=============

Dependencies
------------

  * C++14 compiler
  * geant4
  * ROOT6
  * clasdigi
  * c12sim

Running
-------

### Options

    $ fast-c12sim -h
     usage: c12sim [options] [macro file]    
     Options:                               
         -r, --run=NUMBER         Set simulation "run" NUMBER
         -i, --input=FILE         Set the input file from which events will be read
         -o, --output=NAME        Set the output file name which will have the run number appended
         -n, --events=#           Causes the execution of the ui command "/run/beamOn #".
                                  This happens just before exiting (in the case of batch mode) or returning to UI prompt.
                                  Default is "clas12sim". Note this is just the file basename; use -D to set the directory 
         -D, --dir=NAME           Set the output directory. The default is "data/rootfiles/"
         -t, --treename=NAME      Set the output tree name. The default is "clasdigi_hits"
         -g, --gui=#              Set to 1 (default) to use qt gui or
                                  0 to use command line
         -V, --vis=#              set to 1 (default) to visualization geometry and events
                                  0 to turn off visualization
         -I, --interactive        run in interactive mode (default)
         -N, --init               run without initializing G4 kernel
         -b, --batch              run in batch mode
         -f, --field-dir          print the field dir
         -F, --dl-field-maps      downloads the field maps into the field dir
         -R, --rand=NUMBER        set the random number seed
         -S, --solenoid-field=B   Scale or turn off solenoid field
         -T, --toroid-field=B     Scale or turn off solenoid field

### Examples 

Running without the solenoid and toroid fields:

    $ fast-c12sim -S 0 -T 0 
    $ fast-c12sim --solenoid-field=0 --toroid-field=0 

Running with the solenoid but not the toroid field:

    $ fast-c12sim -T 0 
    $ fast-c12sim --toroid-field=0 

Running with fields reversed and at half their nominal values:

    $ fast-c12sim -T -0.5 -S -0.5 
    $ fast-c12sim --solenoid-field=-0.5 --toroid-field=-0.5

Running in batch mode with piped G4UI commands:

    $ echo "/run/beamOn 10000" | fast-c12sim --run=123 -b


Full Example
------------

Here is a full example including the basic setup

    git clone /group/clas12-alert/repos/alert_fastmc.git
    cd alert_fastmc
    mkdir data
    cd    data
    ln -s /group/clas12-alert/local/share/c12sim/data/lundfiles
    cd ..
    mkdir -p data/results/fastmc
    echo "/run/beamOn 10000" | fast-c12sim -i data/lundfiles/eg_He4_4002.lund --run=50 -b
    root -b -q "scripts/alert/alert_smearing.cxx(50)"
    root -b -q "scripts/alert/fastmc_resolutions.cxx(50)"

The scripts <tt>alert_smearing.cxx</tt> and <tt>fastmc_resolution</tt> should 
be used as templates for more sophisticated smearing/analysis.


The scripts found in <tt>scripts/alert</tt> perform the smearing and help 
analyze the results.

<tt>scripts/alert/alert_smearing.cxx</tt>


Bugs and Requests
-----------------

Please feel free to report bugs here or just email me (whit@jlab.org).
