#include "ThrownEvent.h"
#include "CLAS12HitsEvent.h"
#include "ForwardHitMask.h"
#include "ElectronKinematics.h"
#include "CLHEP/Units/SystemOfUnits.h"

#include "../pid_indexing.h"
#include "../ALERTResolution.h"
#include "../CLAS12Resolution.h"

// ----------------------------------------------------------------------
//
// alpha     = 1000020040
// He3       = 1000020030
// triton    = 1000010030
// deuteron  = 1000010020
// proton    = 2212
//
void electron_kinematics(int runnumber = 2021)
{
   using namespace clas12::hits;
   using namespace CLHEP;

   std::string filename     = Form("data/rootfiles/c12sim_smeared_%d.root",runnumber);
   std::string filename_out = Form("data/rootfiles/c12_smeared_%d.root",runnumber);

   // ----------------------------------------------------------------------
   //
   clas12::hits::CLAS12HitsEvent * event   = 0;
   clas12::sim::ThrownEvent      * thrown  = 0;//new clas12::sim::ThrownEvent();
   clas12::sim::ThrownEvent      * smeared = new clas12::sim::ThrownEvent();
   kinematics::ElectronKinematics * eKine0  = new kinematics::ElectronKinematics();
   kinematics::ElectronKinematics * eKine  = new kinematics::ElectronKinematics();

   //t->SetBranchAddress("HitsEvent",   &event);
   //t->SetBranchAddress("PrimaryEvent",&thrown);

   // ----------------------------------------------------------------------
   //
   TFile * f = new TFile(filename.c_str(),"READ");
   f->cd();

   TTree * t = (TTree*)gROOT->FindObject("FastMC"); 
   Int_t alert_accepted = 0;
   t->SetBranchAddress("HitsEvent", &event);
   t->SetBranchAddress("Thrown",    &thrown);
   t->SetBranchAddress("Smeared",   &smeared);
   t->SetBranchAddress("eKine0",     &eKine0);
   t->SetBranchAddress("eKine",     &eKine);

   TRandom3 rand;
   rand.SetSeed();

   // ----------------------------------------------------------------------
   //
   TFile * fout = new TFile(filename_out.c_str(),"UPDATE");
   

   std::vector<TH1F*> hDeltaE = {
      new TH1F("hDeltaE0", "E", 100, -1, 1),
      new TH1F("hDeltaE1", "E", 100, -1, 1),
      new TH1F("hDeltaE2", "E", 100, -1, 1),
      new TH1F("hDeltaE3", "E", 100, -1, 1),
      new TH1F("hDeltaE4", "E", 100, -1, 1)
   };
   std::vector<TH1F*> hDeltaTheta = {
      new TH1F("hDeltaTheta0", "#Delta#theta", 100, -10, 10),
      new TH1F("hDeltaTheta1", "#Delta#theta", 100, -10, 10),
      new TH1F("hDeltaTheta2", "#Delta#theta", 100, -10, 10),
      new TH1F("hDeltaTheta3", "#Delta#theta", 100, -10, 10),
      new TH1F("hDeltaTheta4", "#Delta#theta", 100, -10, 10)
   };

   std::vector<TH1F*> hPhi = {
      new TH1F("hPhi0", "#phi", 100, 0, 360),
      new TH1F("hPhi1", "#phi", 100, 0, 360),
      new TH1F("hPhi2", "#phi", 100, 0, 360),
      new TH1F("hPhi3", "#phi", 100, 0, 360),
      new TH1F("hPhi4", "#phi", 100, 0, 360)
   };

   std::vector<TH2F*> hThetaPhi = {
      new TH2F("hThetaPhi0", "#theta vs #phi", 180, 0, 360, 70, 0, 70),
      new TH2F("hThetaPhi1", "#theta vs #phi", 180, 0, 360, 70, 0, 70),
      new TH2F("hThetaPhi2", "#theta vs #phi", 180, 0, 360, 70, 0, 70),
      new TH2F("hThetaPhi3", "#theta vs #phi", 180, 0, 360, 90, 0, 180),
      new TH2F("hThetaPhi4", "#theta vs #phi", 180, 0, 360, 90, 0, 180)
   };

   std::vector<TH2F*> hQ2Vsx = {
      new TH2F("hQ2Vsx0", "Q_{2} Vs x", 100, 0, 1, 100, 0, 10),
      new TH2F("hQ2Vsx1", "Q_{2} Vs x", 100, 0, 1, 100, 0, 10),
      new TH2F("hQ2Vsx2", "Q_{2} Vs x", 100,-1, 1, 100,-1, 1),
      new TH2F("hQ2Vsx3", "Q_{2} Vs x", 100, 0, 1, 100, 0, 10),
      new TH2F("hQ2Vsx4", "Q_{2} Vs x", 100, 0, 1, 100, 0, 10)
   };
   std::vector<TH1F*> hE_e = {
      new TH1F("hE0_e", "E", 100, 0, 11),
      new TH1F("hE1_e", "E", 100, 0, 11)
   };
   std::vector<TH1F*> hTheta_e = {
      new TH1F("hTheta0_e", "#theta", 90, 0, 180),
      new TH1F("hTheta1_e", "#theta", 90, 0, 180)
   };
   std::vector<TH1F*> hQ2 = {
      new TH1F("hQ20", "Q^{2}", 100, 0, 12),
      new TH1F("hQ21", "Q^{2}", 100, 0, 12),
      new TH1F("hQ22", "Q^{2}", 100, -0.2, 0.2)
   };
   std::vector<TH1F*> hx = {
      new TH1F("hx0", "x", 100, 0, 1),
      new TH1F("hx1", "x", 100, 0, 1),
      new TH1F("hx2", "x", 100, -0.2, 0.2),
      new TH1F("hx3", "x", 100, 0, 1),
      new TH1F("hx4", "x", 100, 0, 1),
      new TH1F("hx5", "x", 100, -0.2, 0.2),
      new TH1F("hx5", "x", 100, -0.2, 0.2)
   };
   std::vector<TH1F*> hW = {
      new TH1F("hW0", "W", 100, 0.5, 4),
      new TH1F("hW1", "W", 100, 0.5, 4),
      new TH1F("hW2", "W", 100,-0.2, 0.2),
      new TH1F("hW3", "W", 100, 0.5, 4),
      new TH1F("hW4", "W", 100, 0.5, 4),
      new TH1F("hW5", "W", 100,-0.2, 0.2),
      new TH1F("hW5", "W", 100,-0.2, 0.2)
   };
   std::vector<TH1F*> hE0 = {
      new TH1F("hE00", "E_{#gamma}", 100, 0.0, 4),
      new TH1F("hE01", "E_{#gamma}", 100, 0.0, 4)
   };

   // ----------------------------------------------------------------------
   // Event loop 
   int nevents = t->GetEntries();
   std::cout << nevents << " Events." << std::endl;
   for(int ievent = 0; ievent < nevents; ievent++){

      t->GetEntry(ievent);

      int n_thrown  = thrown->fNParticles;

      int status = 0;

      bool good_proton  = false;
      bool good_gamma   = false;
      bool good_e       = false;
      bool good_triton  = false;

      TLorentzVector  p_tot = {0,0,0.0,0.0};
      // ------------------------------------------------------------------
      // 
      for(int ithrown = 0; ithrown<n_thrown; ithrown++){

         auto p0 = thrown->GetParticle( ithrown);
         auto p1 = smeared->GetParticle(ithrown);
         //std::cout << p0->GetPdgCode() << std::endl;;

         // hit mask for a given track
         auto hm = event->TrackHitMask(ithrown);
         if(!hm) continue;

         bool good_dc  = false;
         bool good_rc  = false;
         bool good_rh  = false;
         bool good_all = false;

         if( hm->fDC >30) {
            // A prefect DC track will have a hit in every layer and therefore
            // should have 6*6=36 hits.
            good_dc = true;
         }
         if( hm->fRC >7) {
            // Simlarly the recoil chamber should have a nearly full set of wire hits.
            good_rc = true;
         }
         if( hm->fRH >0) {
            // the recoil hodosocpe should have at least one hit
            good_rh = true;
         }
         if( good_dc && good_rh && good_rc ){
            good_all = true;
         }


         if( p0->GetPdgCode() == 11 ) {

            hE_e       [0]->Fill( p0->Energy() );
            hTheta_e   [0]->Fill( p0->Theta()/degree );

            if(good_dc){

               status++;
               good_e = true;

               hE_e       [1]->Fill(p1->Energy() );
               hTheta_e   [1]->Fill(p1->Theta()/degree );

               hThetaPhi  [0]->Fill(p1->Phi()/degree, p0->Theta()/degree);
               hPhi       [0]->Fill(p1->Phi()/degree);

               hDeltaE    [0]->Fill(p0->P()- p1->P());
               hDeltaTheta[0]->Fill(p0->Theta()/degree -p1->Theta()/degree);
            }

         }
      }


      // -------------------------------------------------
      //
      if( good_e ) {

         double x_thrown = eKine0->x(); 
         double x_recon  = eKine->x(); 

         hx    [0]->Fill(x_thrown );
         hQ2   [0]->Fill(eKine0->Q2() );
         hW    [0]->Fill(eKine0->W() );
         hQ2Vsx[0]->Fill(x_thrown, eKine0->Q2());

         hx    [1]->Fill(eKine->x() );
         hQ2   [1]->Fill(eKine->Q2() );
         hW    [1]->Fill(eKine->W() );
         hQ2Vsx[1]->Fill(x_recon, eKine->Q2());

         hx [2]->Fill( (x_thrown - x_recon)/x_thrown  );
         hQ2[2]->Fill( (eKine0->Q2()-eKine->Q2())/eKine0->Q2() );
         hW [2]->Fill( (eKine0->W() - eKine->W())/eKine0->W()  );
      }
   }
   fout->Write();

   // ----------------------------------------------------------------------
   // 
   TCanvas * c   = 0;
   TLegend * leg = 0;
   THStack * hs  = 0;
   std::vector<int> colors = {1,2,4,6,7,8,9,30,34,37,41,46};

   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.3, 0.7, 0.6, 0.9);
   hs  = new THStack("energies","P");
   gPad->SetLogy(true);
   for(int i = 0; i<2; i++) {
      hE_e[i]->SetLineWidth(2);
      hE_e[i]->SetLineColor(colors[i]);
      hs->Add(hE_e[i]);
   }
   leg->SetNColumns(2);
   leg->AddEntry(hE_e[0], "e- thrown",     "l");
   leg->AddEntry(hE_e[1], "e- smeared",     "l");
   hs->Draw("nostack");
   hs->GetXaxis()->CenterTitle(true);
   hs->GetYaxis()->CenterTitle(true);
   hs->GetXaxis()->SetTitle("P [GeV/c]");
   hs->GetYaxis()->SetTitle("");
   leg->Draw();
   c->SaveAs(Form("data/results/fastmc/electron_kinematics_%d_0.png",runnumber));
   c->SaveAs(Form("data/results/fastmc/electron_kinematics_%d_0.pdf",runnumber));

   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.65, 0.65, 0.85, 0.9);
   hs  = new THStack("theta_hists","#theta");
   gPad->SetLogy(true);
   for(int i = 0; i<2; i++) {
      hTheta_e[i]->SetLineWidth(2);
      hTheta_e[i]->SetLineColor(colors[i]);
      hs->Add(hTheta_e[i]);
   }
   leg->SetNColumns(2);
   leg->AddEntry(hTheta_e[0], "e- thrown",     "l");
   leg->AddEntry(hTheta_e[1], "e- smeared",     "l");
   hs->Draw("nostack");
   hs->GetXaxis()->CenterTitle(true);
   hs->GetYaxis()->CenterTitle(true);
   hs->GetXaxis()->SetTitle("#theta [deg]");
   hs->GetYaxis()->SetTitle("");
   leg->Draw();
   c->SaveAs(Form("data/results/fastmc/electron_kinematics_%d_1.png",runnumber));
   c->SaveAs(Form("data/results/fastmc/electron_kinematics_%d_1.pdf",runnumber));

   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hs  = new THStack("Whists","W");

   for(int i = 0; i<2; i++) {
      hW[i]->SetLineColor(colors[i]);
      hs->Add(hW[i]);
   }
   leg->AddEntry(hW[0],"thrown");
   leg->AddEntry(hW[1],"recon");
   hs->Draw("nostack");
   hs->GetXaxis()->CenterTitle(true);
   hs->GetYaxis()->CenterTitle(true);
   hs->GetXaxis()->SetTitle("W [GeV]");
   hs->GetYaxis()->SetTitle("");
   leg->Draw();
   c->SaveAs(Form("data/results/fastmc/electron_kinematics_%d_2.png",runnumber));
   c->SaveAs(Form("data/results/fastmc/electron_kinematics_%d_2.pdf",runnumber));

   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hs  = new THStack("xhists","x");

   for(int i = 0; i<2; i++) {
      hx[i]->SetLineColor(colors[i]);
      hs->Add(hx[i]);
   }
   leg->AddEntry(hx[0],"thrown");
   leg->AddEntry(hx[1],"recon");
   hs->Draw("nostack");
   hs->GetXaxis()->CenterTitle(true);
   hs->GetYaxis()->CenterTitle(true);
   hs->GetXaxis()->SetTitle("x_{B}");
   hs->GetYaxis()->SetTitle("");
   leg->Draw();
   c->SaveAs(Form("data/results/fastmc/electron_kinematics_%d_3.png",runnumber));
   c->SaveAs(Form("data/results/fastmc/electron_kinematics_%d_3.pdf",runnumber));

   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hs  = new THStack("Q2hists","Q^{2}");

   for(int i = 0; i<2; i++) {
      hQ2[i]->SetLineColor(colors[i]);
      hs->Add(hQ2[i]);
   }
   leg->AddEntry(hQ2[0],"thrown");
   leg->AddEntry(hQ2[1],"recon");
   hs->Draw("nostack");
   hs->GetXaxis()->CenterTitle(true);
   hs->GetYaxis()->CenterTitle(true);
   hs->GetXaxis()->SetTitle("Q^{2} [GeV^{2}/c^{2}]");
   hs->GetYaxis()->SetTitle("");
   leg->Draw();
   c->SaveAs(Form("data/results/fastmc/electron_kinematics_%d_4.png",runnumber));
   c->SaveAs(Form("data/results/fastmc/electron_kinematics_%d_4.pdf",runnumber));

   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hs  = new THStack("deltaEhists","#Delta P");

   hDeltaE[0]->SetLineColor(colors[0]);
   hs->Add(hDeltaE[0]);
   leg->AddEntry(hDeltaE[0],"electrons");
   hs->Draw("nostack");
   hs->GetXaxis()->CenterTitle(true);
   hs->GetYaxis()->CenterTitle(true);
   hs->GetXaxis()->SetTitle("P_{thrown} - P_{recon} [GeV/c]");
   hs->GetYaxis()->SetTitle("");
   leg->Draw();
   c->SaveAs(Form("data/results/fastmc/electron_kinematics_%d_6.png",runnumber));
   c->SaveAs(Form("data/results/fastmc/electron_kinematics_%d_6.pdf",runnumber));

   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hs  = new THStack("deltaTHetahists","#Delta #theta");

   hDeltaTheta[0]->SetLineColor(colors[0]);
   leg->AddEntry(hDeltaTheta[0],"electrons");
   hs->Add(hDeltaTheta[0]);
   hs->Draw("nostack");
   hs->GetXaxis()->CenterTitle(true);
   hs->GetYaxis()->CenterTitle(true);
   hs->GetXaxis()->SetTitle("#theta_{thrown} - #theta_{recon} [deg]");
   hs->GetYaxis()->SetTitle("");
   leg->Draw();
   c->SaveAs(Form("data/results/fastmc/electron_kinematics_%d_7.png",runnumber));
   c->SaveAs(Form("data/results/fastmc/electron_kinematics_%d_7.pdf",runnumber));

   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hThetaPhi[0]->Draw("colz");
   hThetaPhi[0]->GetXaxis()->CenterTitle(true);
   hThetaPhi[0]->GetYaxis()->CenterTitle(true);
   hThetaPhi[0]->GetXaxis()->SetTitle("#phi [deg]");
   hThetaPhi[0]->GetYaxis()->SetTitle("#theta [deg]");
   c->SaveAs(Form("data/results/fastmc/electron_kinematics_%d_8.png",runnumber));
   c->SaveAs(Form("data/results/fastmc/electron_kinematics_%d_8.pdf",runnumber));


   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hs  = new THStack("phihists","#phi");
   hPhi[0]->SetLineColor(colors[0]);
   hs->Add(hPhi[0]);
   hs->Draw("nostack");
   hs->GetXaxis()->CenterTitle(true);
   hs->GetXaxis()->SetTitle("#phi [deg]");
   c->SaveAs(Form("data/results/fastmc/electron_kinematics_%d_9.png",runnumber));
   c->SaveAs(Form("data/results/fastmc/electron_kinematics_%d_9.pdf",runnumber));

   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hx[2]->SetTitle("#sigma x");
   hx[2]->SetLineColor(1);
   hx[2]->Draw();
   leg->AddEntry(hx[2],"exact","l");
   hx[2]->GetXaxis()->CenterTitle(true);
   hx[2]->GetYaxis()->CenterTitle(true);
   hx[2]->GetXaxis()->SetTitle("(x_{thrown}-x_{recon})/x_{thrown}");
   hx[2]->GetYaxis()->SetTitle("");
   leg->Draw();
   c->SaveAs(Form("data/results/fastmc/electron_kinematics_%d_10.png",runnumber));
   c->SaveAs(Form("data/results/fastmc/electron_kinematics_%d_10.pdf",runnumber));

   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hQ2[2]->SetTitle("#sigma Q^{2}");
   hQ2[2]->Draw();
   hQ2[2]->GetXaxis()->CenterTitle(true);
   hQ2[2]->GetYaxis()->CenterTitle(true);
   hQ2[2]->GetXaxis()->SetTitle("(Q^{2}_{thrown}-Q^{2}_{recon})/Q^{2}_{thrown}");
   hQ2[2]->GetYaxis()->SetTitle("");
   c->SaveAs(Form("data/results/fastmc/electron_kinematics_%d_12.png",runnumber));
   c->SaveAs(Form("data/results/fastmc/electron_kinematics_%d_12.pdf",runnumber));

   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hW[2]->Draw();
   hW[2]->GetXaxis()->CenterTitle(true);
   hW[2]->GetYaxis()->CenterTitle(true);
   hW[2]->GetXaxis()->SetTitle("(W_{thrown}-W_{recon})/W_{thrown}");
   hW[2]->GetYaxis()->SetTitle("");
   c->SaveAs(Form("data/results/fastmc/electron_kinematics_%d_13.png",runnumber));
   c->SaveAs(Form("data/results/fastmc/electron_kinematics_%d_13.pdf",runnumber));

   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hQ2Vsx[0]->Draw("colz");
   hQ2Vsx[0]->GetXaxis()->CenterTitle(true);
   hQ2Vsx[0]->GetYaxis()->CenterTitle(true);
   hQ2Vsx[0]->GetXaxis()->SetTitle("x_{B}");
   hQ2Vsx[0]->GetYaxis()->SetTitle("Q^{2} [GeV^{2}]");
   c->SaveAs(Form("data/results/fastmc/electron_kinematics_%d_14.png",runnumber));
   c->SaveAs(Form("data/results/fastmc/electron_kinematics_%d_14.pdf",runnumber));

}


