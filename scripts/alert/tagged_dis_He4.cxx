#include "ThrownEvent.h"
#include "CLAS12HitsEvent.h"
#include "ForwardHitMask.h"
#include "ElectronKinematics.h"
#include "CLHEP/Units/SystemOfUnits.h"

#include "../pid_indexing.h"
#include "../ALERTResolution.h"
#include "../CLAS12Resolution.h"

// ----------------------------------------------------------------------
//
// alpha     = 1000020040
// He3       = 1000020030
// triton    = 1000010030
// deuteron  = 1000010020
// proton    = 2212
//
void fastmc_tagged_dis_results(int runnumber = 50)
{
   using namespace clas12::hits;
   using namespace CLHEP;

   std::string filename     = Form("data/rootfiles/c12sim_smeared_%d.root",runnumber);
   std::string filename_out = Form("data/rootfiles/c12_smeared_%d.root",runnumber);

   // ----------------------------------------------------------------------
   //
   clas12::hits::CLAS12HitsEvent * event   = 0;
   clas12::sim::ThrownEvent      * thrown  = 0;//new clas12::sim::ThrownEvent();
   clas12::sim::ThrownEvent      * smeared = new clas12::sim::ThrownEvent();
   kinematics::ElectronKinematics * eKine0  = new kinematics::ElectronKinematics();
   kinematics::ElectronKinematics * eKine  = new kinematics::ElectronKinematics();

   //t->SetBranchAddress("HitsEvent",   &event);
   //t->SetBranchAddress("PrimaryEvent",&thrown);

   // ----------------------------------------------------------------------
   //
   TFile * f = new TFile(filename.c_str(),"READ");
   f->cd();

   TTree * t = (TTree*)gROOT->FindObject("FastMC"); 
   Int_t alert_accepted = 0;
   t->SetBranchAddress("HitsEvent", &event);
   t->SetBranchAddress("Thrown",    &thrown);
   t->SetBranchAddress("Smeared",   &smeared);
   t->SetBranchAddress("eKine0",     &eKine0);
   t->SetBranchAddress("eKine",     &eKine);

   TRandom3 rand;
   rand.SetSeed();

   // ----------------------------------------------------------------------
   //
   TFile * fout = new TFile(filename_out.c_str(),"UPDATE");
   

   std::vector<TH1F*> hDeltaE = {
      new TH1F("hDeltaE0", "E", 100, -1, 1),
      new TH1F("hDeltaE1", "E", 100, -1, 1),
      new TH1F("hDeltaE2", "E", 100, -1, 1),
      new TH1F("hDeltaE3", "E", 100, -1, 1),
      new TH1F("hDeltaE4", "E", 100, -1, 1)
   };
   std::vector<TH1F*> hDeltaTheta = {
      new TH1F("hDeltaTheta0", "#Delta#theta", 100, -10, 10),
      new TH1F("hDeltaTheta1", "#Delta#theta", 100, -10, 10),
      new TH1F("hDeltaTheta2", "#Delta#theta", 100, -10, 10),
      new TH1F("hDeltaTheta3", "#Delta#theta", 100, -10, 10),
      new TH1F("hDeltaTheta4", "#Delta#theta", 100, -10, 10)
   };

   std::vector<TH1F*> hPhi = {
      new TH1F("hPhi0", "#phi", 100, 0, 360),
      new TH1F("hPhi1", "#phi", 100, 0, 360),
      new TH1F("hPhi2", "#phi", 100, 0, 360),
      new TH1F("hPhi3", "#phi", 100, 0, 360),
      new TH1F("hPhi4", "#phi", 100, 0, 360)
   };

   std::vector<TH2F*> hThetaPhi = {
      new TH2F("hThetaPhi0", "#theta vs #phi", 180, 0, 360, 70, 0, 70),
      new TH2F("hThetaPhi1", "#theta vs #phi", 180, 0, 360, 70, 0, 70),
      new TH2F("hThetaPhi2", "#theta vs #phi", 180, 0, 360, 70, 0, 70),
      new TH2F("hThetaPhi3", "#theta vs #phi", 180, 0, 360, 90, 0, 180),
      new TH2F("hThetaPhi4", "#theta vs #phi", 180, 0, 360, 90, 0, 180)
   };

   std::vector<TH2F*> hP1Vsx = {
      new TH2F("hP1Vsx0", "P_{1} Vs x", 50,0,1, 50, 0, 1),
      new TH2F("hP1Vsx1", "P_{1} Vs x", 50,0,1, 50, 0, 1),
      new TH2F("hP1Vsx2", "P_{1} Vs x", 50,-1,1, 50, -1, 1),
      new TH2F("hP1Vsx3", "P_{1} Vs x", 50,0,1, 50, 0, 1),
      new TH2F("hP1Vsx4", "P_{1} Vs x", 50,0,1, 50, 0, 1)
   };
   std::vector<TH2F*> hP1Vst = {
      new TH2F("hP1Vst0", "P_{1} Vs t", 50,-15,5, 60, 0, 180),
      new TH2F("hP1Vst1", "P_{1} Vs t", 50,-15,5, 60, 0, 180),
      new TH2F("hP1Vst2", "P_{1} Vs t", 50,-15,5, 60, 0, 180),
      new TH2F("hP1Vst3", "P_{1} Vs t", 50,  0,1, 60, 0, 180),
      new TH2F("hP1Vst4", "P_{1} Vs t", 50,  0,1, 60, 0, 180)
   };
   std::vector<TH1F*> hE_e = {
      new TH1F("hE0_e", "E", 100, 0, 11),
      new TH1F("hE1_e", "E", 100, 0, 11)
   };
   std::vector<TH1F*> hE_He4 = {
      new TH1F("hE_He40", "E", 100, 0, 11),
      new TH1F("hE_He41", "E", 100, 0, 11)
   };
   std::vector<TH1F*> hE_p = {
      new TH1F("hE0_p", "E proton", 100, 0, 11.0),
      new TH1F("hE1_p", "E proton", 100, 0, 11.0)
   };
   std::vector<TH1F*> hE_g = {
      new TH1F("hE0_g", "E", 100, 0, 11),
      new TH1F("hE1_g", "E", 100, 0, 11)
   };

   std::vector<TH1F*> hTheta_e = {
      new TH1F("hTheta0_e", "#theta", 90, 0, 180),
      new TH1F("hTheta1_e", "#theta", 90, 0, 180)
   };
   std::vector<TH1F*> hTheta_He4 = {
      new TH1F("hTheta0_He4", "#theta", 90, 0, 180),
      new TH1F("hTheta1_He4", "#theta", 90, 0, 180)
   };
   std::vector<TH1F*> hTheta_p = {
      new TH1F("hTheta0_p", "Theta proton", 90, 0, 180),
      new TH1F("hTheta1_p", "Theta proton", 90, 0, 180)
   };
   std::vector<TH1F*> hTheta_g = {
      new TH1F("hTheta0_g", "Theta", 90, 0, 180),
      new TH1F("hTheta1_g", "Theta", 90, 0, 180)
   };

   std::vector<TH1F*> hQ2 = {
      new TH1F("hQ20", "Q^{2}", 100, 0, 12),
      new TH1F("hQ21", "Q^{2}", 100, 0, 12),
      new TH1F("hQ22", "Q^{2}", 100, -0.2, 0.2)
   };
   std::vector<TH1F*> hx = {
      new TH1F("hx0", "x", 100, 0, 1),
      new TH1F("hx1", "x", 100, 0, 1),
      new TH1F("hx2", "x", 100, -0.2, 0.2),
      new TH1F("hx3", "x", 100, 0, 1),
      new TH1F("hx4", "x", 100, 0, 1),
      new TH1F("hx5", "x", 100, -0.2, 0.2),
      new TH1F("hx5", "x", 100, -0.2, 0.2)
   };
   std::vector<TH1F*> hW = {
      new TH1F("hW0", "W", 100, 0.5, 4),
      new TH1F("hW1", "W", 100, 0.5, 4),
      new TH1F("hW2", "W", 100,-0.2, 0.2),
      new TH1F("hW3", "W", 100, 0.5, 4),
      new TH1F("hW4", "W", 100, 0.5, 4),
      new TH1F("hW5", "W", 100,-0.2, 0.2),
      new TH1F("hW5", "W", 100,-0.2, 0.2)
   };
   std::vector<TH1F*> ht = {
      new TH1F("ht0", "t", 100, -2, 0),
      new TH1F("ht1", "t", 100, -2, 0),
      new TH1F("ht2", "t", 100, -15.0,1.2 ),
      new TH1F("ht3", "t", 100, -2, 0),
      new TH1F("ht4", "t", 100, -2, 0),
      new TH1F("ht5", "t", 100, -15.0,1.2 ),
      new TH1F("ht5", "t", 100, -15.0,1.2 )
   };
   std::vector<TH1F*> hE0 = {
      new TH1F("hE00", "E_{#gamma}", 100, 0.0, 4),
      new TH1F("hE01", "E_{#gamma}", 100, 0.0, 4)
   };

   // ----------------------------------------------------------------------
   // Event loop 
   int nevents = t->GetEntries();
   for(int ievent = 0; ievent < nevents; ievent++){

      t->GetEntry(ievent);

      int n_thrown  = thrown->fNParticles;

      int status = 0;

      TLorentzVector  p_tot = {0,0,0.0,0.0};
      // ------------------------------------------------------------------
      // 
      for(int ithrown = 0; ithrown<n_thrown; ithrown++){

         auto p0 = thrown->GetParticle( ithrown);
         auto p1 = smeared->GetParticle(ithrown);
         //std::cout << p0->GetPdgCode() << std::endl;;

         auto hm = event->TrackHitMask(ithrown);
         if(!hm) continue;
         //if( hm->fDC < 18 ) continue;

         TLorentzVector  mom ;
         p0->Momentum( mom );

         if( ithrown != 3 ){
            p_tot += mom;
         }

         bool good_dc  = false;
         bool good_rc  = false;
         bool good_rh  = false;
         bool good_all = false;

         if( hm->fDC >30) {
            good_dc = true;
         }
         if( hm->fRC >7) {
            good_rc = true;
         }
         if( hm->fRH >0) {
            good_rh = true;
         }
         if( good_dc /*&& good_rh && good_rc*/ ){
            good_all = true;
         }


         if( p0->GetPdgCode() == 11 ) {

            hE_e       [0]->Fill( p0->Energy() );
            hTheta_e   [0]->Fill( p0->Theta()/degree );

            if(good_dc){
               status++;
               hE_e       [1]->Fill(p1->Energy() );
               hTheta_e   [1]->Fill(p1->Theta()/degree );

               hThetaPhi  [0]->Fill(p1->Phi()/degree, p0->Theta()/degree);
               hPhi       [0]->Fill(p1->Phi()/degree);

               hDeltaE    [0]->Fill(p0->P()- p1->P());
               hDeltaTheta[0]->Fill(p0->Theta()/degree -p1->Theta()/degree);
            }

         } else if( p0->GetPdgCode() == 2212 ) {

            hE_p    [0]->Fill( p0->Energy() );
            hTheta_p[0]->Fill( p0->Theta()/degree );

            if(good_dc){
               hE_p       [1]->Fill( p1->Energy() );
               hTheta_p   [1]->Fill( p1->Theta()/degree );

               hThetaPhi  [1]->Fill(p1->Phi()/degree,p0->Theta()/degree);
               hPhi       [1]->Fill(p1->Phi()/degree);

               hDeltaE    [1]->Fill(p0->P() - p1->P());
               hDeltaTheta[1]->Fill(p0->Theta()/degree -p1->Theta()/degree);
            }
         } else if( p0->GetPdgCode() == 22 ) {

            hE_g[0]->Fill( p0->Energy() );
            hTheta_g[0]->Fill( p0->Theta()/degree );

            if(good_dc){
               //status++;
               hE_g    [1]->Fill( p1->Energy() );
               hTheta_g[1]->Fill( p1->Theta()/degree );

               hThetaPhi[2]->Fill(p1->Phi()/degree, p1->Theta()/degree);
               hPhi     [2]->Fill(p1->Phi()/degree);

               hDeltaE    [2]->Fill(p0->P()- p1->P());
               hDeltaTheta[2]->Fill(p0->Theta()/degree -p1->Theta()/degree);
            }

         } else if( (p0->GetPdgCode() == 1000010030) || (p0->GetPdgCode() == 1000020040) || (p0->GetPdgCode() == 1000020030) ) {

            //std::cout << " derp\n";
            hE_He4    [0]->Fill( p0->P() );
            hTheta_He4[0]->Fill( p0->Theta()/degree );

            if(good_rh){
               status++;
               hE_He4    [1]->Fill( p1->P() );
               hTheta_He4[1]->Fill( p1->Theta()/degree );

               hThetaPhi[3]->Fill(p1->Phi()/degree, p1->Theta()/degree);
               hPhi     [3]->Fill(p1->Phi()/degree);

               hDeltaE    [3]->Fill(p0->P()- p1->P());
               hDeltaTheta[3]->Fill(p0->Theta()/degree -p1->Theta()/degree);
            }
         }

      }

      //p_tot.Print();

      // -------------------------------------------------
      //
      if( status > 1 ) {
         double P1_thrown = eKine0->p1().Vect().Mag();
         double P1_recon  = eKine->p1().Vect().Mag();

         double P1_theta_thrown = eKine0->p1().Vect().Theta();
         double P1_theta_recon  = eKine->p1().Vect().Theta();

         double x_exact_thrown  = eKine0->x_true();
         double x_exact_recon   = eKine->x_true();
         double x_approx_thrown = eKine0->x(); 
         double x_approx_recon  = eKine->x(); 

         double t_exact_thrown  = eKine0->t_true();
         double t_exact_recon   = eKine->t_true();
         double t_approx_thrown = eKine0->t_approx();
         double t_approx_recon  = eKine->t_approx();

         hx[0]->Fill(  x_exact_thrown );
         hQ2[0]->Fill( eKine0->Q2() );
         hW[0]->Fill(  eKine0->W() );
         ht[0]->Fill(  eKine0->t() );
         hP1Vsx[0]->Fill(x_exact_thrown ,P1_thrown);

         hx[1]->Fill(  eKine->x() );
         hQ2[1]->Fill( eKine->Q2() );
         hW[1]->Fill(  eKine->W() );
         ht[1]->Fill(  eKine->t() );
         hP1Vsx[1]->Fill(x_exact_recon ,P1_recon);

         hx [2]->Fill( (x_exact_thrown - x_exact_recon)/x_exact_thrown  );
         hQ2[2]->Fill( (eKine0->Q2()-eKine->Q2())/eKine0->Q2() );
         hW [2]->Fill( (eKine0->W() - eKine->W())/eKine0->W()  );
         ht [2]->Fill( (eKine0->t() - eKine->t())/eKine0->t()  );

         hP1Vsx[2]->Fill( (x_exact_thrown - x_exact_recon)/x_exact_thrown  ,
                          (P1_thrown - P1_recon)/P1_thrown );

         hP1Vsx[3]->Fill(x_approx_thrown ,P1_thrown);
         hP1Vsx[4]->Fill(x_approx_recon , P1_recon);

         ht[3]->Fill(t_approx_thrown);
         ht[4]->Fill(t_approx_recon);
         ht[5]->Fill((t_approx_thrown-t_approx_recon)/t_approx_thrown);

         ht[6]->Fill((t_exact_recon-t_approx_recon)/t_exact_recon);

         hx[3]->Fill(x_approx_thrown);
         hx[4]->Fill(x_approx_recon);
         hx[5]->Fill((x_approx_thrown-x_approx_recon)/x_approx_thrown);
         hx[6]->Fill((x_exact_recon-x_approx_recon)/x_exact_recon);

         hP1Vst[0]->Fill((t_exact_thrown-t_approx_thrown)/t_exact_thrown, P1_theta_thrown/degree);
         hP1Vst[1]->Fill((t_exact_recon-t_approx_recon)/t_exact_recon, P1_theta_recon/degree);
      }
   }
   fout->Write();

   // ----------------------------------------------------------------------
   // 
   TCanvas * c   = 0;
   TLegend * leg = 0;
   THStack * hs  = 0;
   std::vector<int> colors = {1,2,4,6,7,8,9,30,34,37,41,46};

   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.3, 0.7, 0.6, 0.9);
   hs  = new THStack("energies","P");
   gPad->SetLogy(true);
   for(int i = 0; i<2; i++) {
      hE_e[i]->SetLineWidth(2);
      hE_p[i]->SetLineWidth(2);
      hE_g[i]->SetLineWidth(2);
      hE_He4[i]->SetLineWidth(2);

      hE_e[i]->SetLineColor(colors[i]);
      hE_p[i]->SetLineColor(colors[i+2]);
      hE_g[i]->SetLineColor(colors[i+4]);
      hE_He4[i]->SetLineColor(colors[i+6]);

      hs->Add(hE_e[i]);
      hs->Add(hE_p[i]);
      hs->Add(hE_g[i]);
      hs->Add(hE_He4[i]);
   }
   leg->SetNColumns(2);
   leg->AddEntry(hE_e[0], "e- thrown",     "l");
   leg->AddEntry(hE_e[1], "e- smeared",     "l");
   leg->AddEntry(hE_p[0], "p thrown",      "l");
   leg->AddEntry(hE_p[1], "p  smeared",      "l");
   leg->AddEntry(hE_g[0], "#gamma thrown", "l");
   leg->AddEntry(hE_g[1], "#gamma smeared", "l");
   leg->AddEntry(hE_He4[0], "recoil thrown", "l");
   leg->AddEntry(hE_He4[1], "recoil smeared", "l");
   hs->Draw("nostack");
   hs->GetXaxis()->CenterTitle(true);
   hs->GetYaxis()->CenterTitle(true);
   hs->GetXaxis()->SetTitle("P [GeV/c]");
   hs->GetYaxis()->SetTitle("");
   leg->Draw();
   c->SaveAs(Form("data/results/fastmc/fastmc_tagged_dis_results_%d_0.png",runnumber));
   c->SaveAs(Form("data/results/fastmc/fastmc_tagged_dis_results_%d_0.pdf",runnumber));

   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.65, 0.65, 0.85, 0.9);
   hs  = new THStack("theta_hists","#theta");
   gPad->SetLogy(true);
   for(int i = 0; i<2; i++) {
      hTheta_e[i]->SetLineWidth(2);
      hTheta_p[i]->SetLineWidth(2);
      hTheta_g[i]->SetLineWidth(2);
      hTheta_He4[i]->SetLineWidth(2);

      hTheta_e[i]->SetLineColor(colors[i]);
      hTheta_p[i]->SetLineColor(colors[i+2]);
      hTheta_g[i]->SetLineColor(colors[i+4]);
      hTheta_He4[i]->SetLineColor(colors[i+6]);

      hs->Add(hTheta_e[i]);
      hs->Add(hTheta_p[i]);
      hs->Add(hTheta_g[i]);
      hs->Add(hTheta_He4[i]);
   }
   leg->SetNColumns(2);
   leg->AddEntry(hTheta_e[0], "e- thrown",     "l");
   leg->AddEntry(hTheta_e[1], "e- smeared",     "l");
   leg->AddEntry(hTheta_p[0], "p thrown",      "l");
   leg->AddEntry(hTheta_p[1], "p  smeared",      "l");
   leg->AddEntry(hTheta_g[0], "#gamma thrown", "l");
   leg->AddEntry(hTheta_g[1], "#gamma smeared", "l");
   leg->AddEntry(hTheta_He4[0], "recoil thrown", "l");
   leg->AddEntry(hTheta_He4[1], "recoil smeared", "l");
   hs->Draw("nostack");
   hs->GetXaxis()->CenterTitle(true);
   hs->GetYaxis()->CenterTitle(true);
   hs->GetXaxis()->SetTitle("#theta [deg]");
   hs->GetYaxis()->SetTitle("");
   leg->Draw();
   c->SaveAs(Form("data/results/fastmc/fastmc_tagged_dis_results_%d_1.png",runnumber));
   c->SaveAs(Form("data/results/fastmc/fastmc_tagged_dis_results_%d_1.pdf",runnumber));

   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hs  = new THStack("Whists","W");

   for(int i = 0; i<2; i++) {
      hW[i]->SetLineColor(colors[i]);
      hs->Add(hW[i]);
   }
   leg->AddEntry(hW[0],"thrown");
   leg->AddEntry(hW[1],"recon");
   hs->Draw("nostack");
   hs->GetXaxis()->CenterTitle(true);
   hs->GetYaxis()->CenterTitle(true);
   hs->GetXaxis()->SetTitle("W [GeV]");
   hs->GetYaxis()->SetTitle("");
   leg->Draw();
   c->SaveAs(Form("data/results/fastmc/fastmc_tagged_dis_results_%d_2.png",runnumber));
   c->SaveAs(Form("data/results/fastmc/fastmc_tagged_dis_results_%d_2.pdf",runnumber));

   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hs  = new THStack("xhists","x");

   for(int i = 0; i<2; i++) {
      hx[i]->SetLineColor(colors[i]);
      hs->Add(hx[i]);
   }
   for(int i = 3; i<5; i++) {
      hx[i]->SetLineColor(colors[i]);
      hs->Add(hx[i]);
   }
   leg->AddEntry(hx[0],"exact thrown");
   leg->AddEntry(hx[1],"exact recon");
   leg->AddEntry(hx[3],"approx thrown");
   leg->AddEntry(hx[4],"approx recon");
   hs->Draw("nostack");
   hs->GetXaxis()->CenterTitle(true);
   hs->GetYaxis()->CenterTitle(true);
   hs->GetXaxis()->SetTitle("x_{B}");
   hs->GetYaxis()->SetTitle("");
   leg->Draw();
   c->SaveAs(Form("data/results/fastmc/fastmc_tagged_dis_results_%d_3.png",runnumber));
   c->SaveAs(Form("data/results/fastmc/fastmc_tagged_dis_results_%d_3.pdf",runnumber));

   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hs  = new THStack("Q2hists","Q^{2}");

   for(int i = 0; i<2; i++) {
      hQ2[i]->SetLineColor(colors[i]);
      hs->Add(hQ2[i]);
   }
   leg->AddEntry(hQ2[0],"thrown");
   leg->AddEntry(hQ2[1],"recon");
   hs->Draw("nostack");
   hs->GetXaxis()->CenterTitle(true);
   hs->GetYaxis()->CenterTitle(true);
   hs->GetXaxis()->SetTitle("Q^{2} [GeV^{2}/c^{2}]");
   hs->GetYaxis()->SetTitle("");
   leg->Draw();
   c->SaveAs(Form("data/results/fastmc/fastmc_tagged_dis_results_%d_4.png",runnumber));
   c->SaveAs(Form("data/results/fastmc/fastmc_tagged_dis_results_%d_4.pdf",runnumber));

   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.2, 0.7, 0.4, 0.9);
   hs  = new THStack("thists","t");
   for(int i = 0; i<2; i++) {
      ht[i]->SetLineColor(colors[i]);
      hs->Add(ht[i]);
   }
   for(int i = 3; i<5; i++) {
      ht[i]->SetLineColor(colors[i]);
      hs->Add(ht[i]);
   }
   leg->AddEntry(ht[0],"thrown");
   leg->AddEntry(ht[1],"recon");
   leg->AddEntry(ht[3],"approx thrown");
   leg->AddEntry(ht[4],"approx recon");
   hs->Draw("nostack");
   hs->GetXaxis()->CenterTitle(true);
   hs->GetYaxis()->CenterTitle(true);
   hs->GetXaxis()->SetTitle("t [GeV^{2}]");
   hs->GetYaxis()->SetTitle("");
   leg->Draw();
   c->SaveAs(Form("data/results/fastmc/fastmc_tagged_dis_results_%d_5.png",runnumber));
   c->SaveAs(Form("data/results/fastmc/fastmc_tagged_dis_results_%d_5.pdf",runnumber));

   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hs  = new THStack("deltaEhists","#Delta P");

   for(int i = 0; i<3; i++) {
      hDeltaE[i]->SetLineColor(colors[i]);
      hs->Add(hDeltaE[i]);
   }
   leg->AddEntry(hDeltaE[0],"electrons");
   leg->AddEntry(hDeltaE[2],"photons");
   leg->Draw();
   hs->Draw("nostack");
   hs->GetXaxis()->CenterTitle(true);
   hs->GetYaxis()->CenterTitle(true);
   hs->GetXaxis()->SetTitle("P_{thrown} - P_{recon} [GeV]");
   hs->GetYaxis()->SetTitle("");
   c->SaveAs(Form("data/results/fastmc/fastmc_tagged_dis_results_%d_6.png",runnumber));
   c->SaveAs(Form("data/results/fastmc/fastmc_tagged_dis_results_%d_6.pdf",runnumber));

   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hs  = new THStack("deltaTHetahists","#Delta #theta");

   for(int i = 0; i<3; i++) {
      hDeltaTheta[i]->SetLineColor(colors[i]);
      hs->Add(hDeltaTheta[i]);
   }
   hs->Draw("nostack");
   hs->Draw("colz");
   hs->GetXaxis()->CenterTitle(true);
   hs->GetYaxis()->CenterTitle(true);
   hs->GetXaxis()->SetTitle("#theta_{thrown} - #theta_{recon} [deg]");
   hs->GetYaxis()->SetTitle("");
   c->SaveAs(Form("data/results/fastmc/fastmc_tagged_dis_results_%d_7.png",runnumber));
   c->SaveAs(Form("data/results/fastmc/fastmc_tagged_dis_results_%d_7.pdf",runnumber));

   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hThetaPhi[0]->Draw("colz");
   hThetaPhi[0]->GetXaxis()->CenterTitle(true);
   hThetaPhi[0]->GetYaxis()->CenterTitle(true);
   hThetaPhi[0]->GetXaxis()->SetTitle("#phi [deg]");
   hThetaPhi[0]->GetYaxis()->SetTitle("#theta [deg]");
   c->SaveAs(Form("data/results/fastmc/fastmc_tagged_dis_results_%d_8.png",runnumber));
   c->SaveAs(Form("data/results/fastmc/fastmc_tagged_dis_results_%d_8.pdf",runnumber));

   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hThetaPhi[1]->Draw("colz");
   hThetaPhi[1]->GetXaxis()->CenterTitle(true);
   hThetaPhi[1]->GetYaxis()->CenterTitle(true);
   hThetaPhi[1]->GetXaxis()->SetTitle("#phi [deg]");
   hThetaPhi[1]->GetYaxis()->SetTitle("#theta [deg]");
   c->SaveAs(Form("data/results/fastmc/fastmc_tagged_dis_results_%d_81.png",runnumber));
   c->SaveAs(Form("data/results/fastmc/fastmc_tagged_dis_results_%d_81.pdf",runnumber));
   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hThetaPhi[2]->Draw("colz");
   hThetaPhi[2]->GetXaxis()->CenterTitle(true);
   hThetaPhi[2]->GetYaxis()->CenterTitle(true);
   hThetaPhi[2]->GetXaxis()->SetTitle("#phi [deg]");
   hThetaPhi[2]->GetYaxis()->SetTitle("#theta [deg]");
   c->SaveAs(Form("data/results/fastmc/fastmc_tagged_dis_results_%d_82.png",runnumber));
   c->SaveAs(Form("data/results/fastmc/fastmc_tagged_dis_results_%d_82.pdf",runnumber));
   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hThetaPhi[3]->Draw("colz");
   hThetaPhi[3]->GetXaxis()->CenterTitle(true);
   hThetaPhi[3]->GetYaxis()->CenterTitle(true);
   hThetaPhi[3]->GetXaxis()->SetTitle("#phi [deg]");
   hThetaPhi[3]->GetYaxis()->SetTitle("#theta [deg]");
   c->SaveAs(Form("data/results/fastmc/fastmc_tagged_dis_results_%d_83.png",runnumber));
   c->SaveAs(Form("data/results/fastmc/fastmc_tagged_dis_results_%d_83.pdf",runnumber));

   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hs  = new THStack("phihists","#phi");

   for(int i = 0; i<3; i++) {
      hPhi[i]->SetLineColor(colors[i]);
      hs->Add(hPhi[i]);
   }
   hs->Draw("nostack");
   c->SaveAs(Form("data/results/fastmc/fastmc_tagged_dis_results_%d_9.png",runnumber));
   c->SaveAs(Form("data/results/fastmc/fastmc_tagged_dis_results_%d_9.pdf",runnumber));

   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hx[2]->SetTitle("#sigma x");
   hx[2]->SetLineColor(1);
   hx[5]->SetLineColor(2);
   hx[6]->SetLineColor(4);
   hx[2]->Draw();
   hx[5]->Draw("same");
   hx[6]->Draw("same");
   leg->AddEntry(hx[2],"exact","l");
   leg->AddEntry(hx[5],"approx","l");
   leg->AddEntry(hx[6],"(exact-approx/exact","l");
   hx[2]->GetXaxis()->CenterTitle(true);
   hx[2]->GetYaxis()->CenterTitle(true);
   hx[2]->GetXaxis()->SetTitle("(x_{thrown}-x_{recon})/x_{thrown}");
   hx[2]->GetYaxis()->SetTitle("");
   leg->Draw();
   c->SaveAs(Form("data/results/fastmc/fastmc_tagged_dis_results_%d_10.png",runnumber));
   c->SaveAs(Form("data/results/fastmc/fastmc_tagged_dis_results_%d_10.pdf",runnumber));
   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.2, 0.7, 0.4, 0.9);
   ht[2]->SetTitle("#sigma t");
   ht[2]->SetLineColor(1);
   ht[5]->SetLineColor(2);
   ht[6]->SetLineColor(4);
   ht[2]->Draw();
   ht[5]->Draw("same");
   ht[6]->Draw("same");
   leg->AddEntry(ht[2],"exact","l");
   leg->AddEntry(ht[5],"approx","l");
   leg->AddEntry(ht[6],"(exact-approx/exact","l");
   ht[2]->GetXaxis()->CenterTitle(true);
   ht[2]->GetYaxis()->CenterTitle(true);
   ht[2]->GetXaxis()->SetTitle("(t_{thrown}-t_{recon})/t_{thrown}");
   ht[2]->GetYaxis()->SetTitle("");
   leg->Draw();
   c->SaveAs(Form("data/results/fastmc/fastmc_tagged_dis_results_%d_11.png",runnumber));
   c->SaveAs(Form("data/results/fastmc/fastmc_tagged_dis_results_%d_11.pdf",runnumber));
   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hQ2[2]->SetTitle("#sigma Q^{2}");
   hQ2[2]->Draw();
   hQ2[2]->GetXaxis()->CenterTitle(true);
   hQ2[2]->GetYaxis()->CenterTitle(true);
   hQ2[2]->GetXaxis()->SetTitle("(Q^{2}_{thrown}-Q^{2}_{recon})/Q^{2}_{thrown}");
   hQ2[2]->GetYaxis()->SetTitle("");
   c->SaveAs(Form("data/results/fastmc/fastmc_tagged_dis_results_%d_12.png",runnumber));
   c->SaveAs(Form("data/results/fastmc/fastmc_tagged_dis_results_%d_12.pdf",runnumber));
   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hW[2]->Draw();
   hW[2]->GetXaxis()->CenterTitle(true);
   hW[2]->GetYaxis()->CenterTitle(true);
   hW[2]->GetXaxis()->SetTitle("(W_{thrown}-W_{recon})/W_{thrown}");
   hW[2]->GetYaxis()->SetTitle("");
   c->SaveAs(Form("data/results/fastmc/fastmc_tagged_dis_results_%d_13.png",runnumber));
   c->SaveAs(Form("data/results/fastmc/fastmc_tagged_dis_results_%d_13.pdf",runnumber));
   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hP1Vsx[2]->Draw("colz");
   hP1Vsx[2]->GetXaxis()->CenterTitle(true);
   hP1Vsx[2]->GetYaxis()->CenterTitle(true);
   hP1Vsx[2]->GetXaxis()->SetTitle("(x_{thrown}-x_{recon})/x_{thrown}");
   hP1Vsx[2]->GetYaxis()->SetTitle("(P1_{thrown}-P1_{recon})/P1_{thrown}");
   c->SaveAs(Form("data/results/fastmc/fastmc_tagged_dis_results_%d_14.png",runnumber));
   c->SaveAs(Form("data/results/fastmc/fastmc_tagged_dis_results_%d_14.pdf",runnumber));

   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hP1Vsx[0]->Draw("colz");
   hP1Vsx[0]->GetXaxis()->CenterTitle(true);
   hP1Vsx[0]->GetYaxis()->CenterTitle(true);
   hP1Vsx[0]->GetXaxis()->SetTitle("x_{thrown}");
   hP1Vsx[0]->GetYaxis()->SetTitle("P1_{thrown}");
   c->SaveAs(Form("data/results/fastmc/fastmc_tagged_dis_results_%d_15.png",runnumber));
   c->SaveAs(Form("data/results/fastmc/fastmc_tagged_dis_results_%d_15.pdf",runnumber));

   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hP1Vsx[1]->Draw("colz");
   hP1Vsx[1]->GetXaxis()->CenterTitle(true);
   hP1Vsx[1]->GetYaxis()->CenterTitle(true);
   hP1Vsx[1]->GetXaxis()->SetTitle("x_{recon}");
   hP1Vsx[1]->GetYaxis()->SetTitle("P1_{recon}");
   c->SaveAs(Form("data/results/fastmc/fastmc_tagged_dis_results_%d_16.png",runnumber));
   c->SaveAs(Form("data/results/fastmc/fastmc_tagged_dis_results_%d_16.pdf",runnumber));

   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hP1Vsx[3]->Draw("colz");
   hP1Vsx[3]->GetXaxis()->CenterTitle(true);
   hP1Vsx[3]->GetYaxis()->CenterTitle(true);
   hP1Vsx[3]->GetXaxis()->SetTitle("x_{Tagged} = Q^{2}/2p_{1}q");
   hP1Vsx[3]->GetYaxis()->SetTitle("P1_{thrown}");
   c->SaveAs(Form("data/results/fastmc/fastmc_tagged_dis_results_%d_17.png",runnumber));
   c->SaveAs(Form("data/results/fastmc/fastmc_tagged_dis_results_%d_17.pdf",runnumber));
   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hP1Vsx[4]->Draw("colz");
   hP1Vsx[4]->GetXaxis()->CenterTitle(true);
   hP1Vsx[4]->GetYaxis()->CenterTitle(true);
   hP1Vsx[4]->GetXaxis()->SetTitle("x_{Tagged} = Q^{2}/2p_{1}q");
   hP1Vsx[4]->GetYaxis()->SetTitle("P1_{recon}");
   c->SaveAs(Form("data/results/fastmc/fastmc_tagged_dis_results_%d_18.png",runnumber));
   c->SaveAs(Form("data/results/fastmc/fastmc_tagged_dis_results_%d_18.pdf",runnumber));

   // ---------------------------------------------
   c   = new TCanvas();
   leg = new TLegend(0.7, 0.7, 0.9, 0.9);
   hP1Vst[0]->Draw("colz");
   hP1Vst[0]->GetXaxis()->CenterTitle(true);
   hP1Vst[0]->GetYaxis()->CenterTitle(true);
   hP1Vst[0]->GetXaxis()->SetTitle("t_{approx}-t_{exact}");
   hP1Vst[0]->GetYaxis()->SetTitle("#theta_{P1} [deg]");
   c->SaveAs(Form("data/results/fastmc/fastmc_tagged_dis_results_%d_19.png",runnumber));
   c->SaveAs(Form("data/results/fastmc/fastmc_tagged_dis_results_%d_19.pdf",runnumber));
}


